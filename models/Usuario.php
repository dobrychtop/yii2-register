<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $usu_id
 * @property string $usu_login
 * @property string $usu_email
 * @property string $usu_senha
 * @property integer $usu_pais
 * @property string $usu_fuso_horario
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usu_login', 'usu_email', 'usu_senha', 'usu_pais', 'usu_fuso_horario'], 'required'],
            [['usu_pais'], 'integer'],
            [['usu_login'], 'string', 'max' => 30],
            [['usu_email', 'usu_fuso_horario'], 'string', 'max' => 50],
            [['usu_senha'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usu_id' => 'Usu ID',
            'usu_login' => 'Usu Login',
            'usu_email' => 'Usu Email',
            'usu_senha' => 'Usu Senha',
            'usu_pais' => 'Usu Pais',
            'usu_fuso_horario' => 'Usu Fuso Horario',
        ];
    }
}
