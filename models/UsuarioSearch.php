<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuario;

/**
 * UsuarioSearch represents the model behind the search form about `app\models\Usuario`.
 */
class UsuarioSearch extends Usuario
{
    public $buscar;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usu_id'], 'integer'],
            [['usu_login', 'usu_email', 'usu_senha', 'usu_fuso_horario', 'usu_pais', 'buscar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'usu_id' => $this->usu_id,
        ]);

        $query->orFilterWhere(['like', 'usu_login', $this->buscar])
            ->orFilterWhere(['like', 'usu_email', $this->buscar])
            ->orFilterWhere(['like', 'usu_senha', $this->usu_senha])
            ->orFilterWhere(['like', 'usu_fuso_horario', $this->usu_fuso_horario])
            ->orFilterWhere(['like', 'usu_pais', $this->usu_pais]);

        return $dataProvider;
    }
}
