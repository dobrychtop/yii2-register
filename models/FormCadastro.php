<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Users;
use yii\helpers\VarDumper;

class FormCadastro extends model{

    //novos
    public $usu_login;
    public $usu_email;
    public $usu_senha;
    public $usu_pais;
    public $usu_fuso_horario;
    public $password_repeat;

    public function rules()
    {
        return [
            [['usu_login', 'usu_email', 'usu_senha', 'usu_pais'], 'required', 'message' => 'Campos obrigatórios'],
            ['usu_login', 'match', 'pattern' => "/^.{3,30}$/", 'message' => 'Minímo de 3 caracteres e máximo de 30'],
            ['usu_login', 'match', 'pattern' => "/^[0-9a-z]+$/i", 'message' => 'Apenas letras e números são aceitos'],
            ['usu_login', 'username_existe'],
            ['usu_email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'Minímo de 5 caracteres e máximo de 50'],
            ['usu_email', 'email', 'message' => 'Formato inválido'],
            ['usu_email', 'email_existe'],
            ['usu_senha', 'match', 'pattern' => "/^.{6,100}$/", 'message' => 'Minímo de 6 caracteres e máximo de 100'],
            ['password_repeat', 'compare', 'compareAttribute' => 'usu_senha', 'message' => 'As senhas estão diferentes'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usu_id' => 'ID do Usuário',
            'usu_login' => 'Login',
            'usu_email' => 'Email',
            'usu_senha' => 'Senha',
            'usu_pais' => 'Pais',
            'usu_fuso_horario' => 'Fuso Horario',
            'password_repeat' => 'Repita Senha',
        ];
    }

    public function email_existe($attribute, $params)
    {

        //Busca o email na tabela
        $table = Users::find()->where("usu_email=:usu_email", [":usu_email" => $this->usu_email]);

        //Se já existe email, mostra o erro
        if ($table->count() == 1)
        {
            $this->addError($attribute, "Já existe um cadastro com este email.");
        }
    }

    public function username_existe($attribute, $params)
    {
        //Busca o usu_login na tabela
        $table = Users::find()->where("usu_login=:usu_login", [":usu_login" => $this->usu_login]);

        //Se ja existir cadastro, mostra erro
        if ($table->count() == 1)
        {
            $this->addError($attribute, "Já existe um cadastro com este login.");
        }
    }

}