<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paises".
 *
 * @property string $iso
 * @property string $iso3
 * @property integer $numcode
 * @property string $nome
 *
 * @property Usuario[] $usuarios
 */
class Pais extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paises';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iso', 'iso3', 'nome'], 'required'],
            [['numcode'], 'integer'],
            [['iso'], 'string', 'max' => 2],
            [['iso3'], 'string', 'max' => 3],
            [['nome'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iso' => 'Iso',
            'iso3' => 'Iso3',
            'numcode' => 'Numcode',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['usu_pais' => 'iso']);
    }
}
