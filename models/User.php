<?php

namespace app\models;

use app\models\Usuario as Usuario;
use yii\helpers\VarDumper;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $usu_id;
    public $username;
    public $usu_login;
    public $usu_email;
    public $usu_senha;
    public $usu_pais;
    public $usu_fuso_horario;
    public $password;
    public $authKey;
    public $accessToken;

//    private static $users = [
//        '100' => [
//            'id' => '100',
//            'username' => 'admin',
//            'password' => 'admin',
//            'authKey' => 'test100key',
//            'accessToken' => '100-token',
//        ],
//        '101' => [
//            'id' => '101',
//            'username' => 'demo',
//            'password' => 'demo',
//            'authKey' => 'test101key',
//            'accessToken' => '101-token',
//        ],
//    ];


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
//        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        $user = Usuario::find()->where(['usu_id'=>$id])->one();

        if($user){
            return new static($user);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        foreach (self::$users as $user) {
//            if ($user['accessToken'] === $token) {
//                return new static($user);
//            }
//        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {

        $user = Usuario::find()->where(['usu_login'=>$username])->one();
        $user2 = Usuario::find()->where(['usu_email'=>$username])->one();

        if($user){
            return new static($user);
        }elseif($user2){
            return new static($user2);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->usu_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
//        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return null;
//        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->usu_senha === $password;
    }
}
