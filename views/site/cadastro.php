<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

    <h3><?= $msg ?></h3>

    <h1>Cadastro</h1>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'formulario',
    'enableClientValidation' => true,
    'enableAjaxValidation' => false,
]);
?>
    <div class="form-group">
        <?= $form->field($model, "usu_login")->input("text") ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, "usu_email")->input("email") ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'usu_pais')->textInput(['maxlength' => 2])->
            label('País'. Html::tag('span', '',['class'=>'required']))->
            dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Pais::find()->All(), 'iso', 'nome')); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->field($model, "usu_senha")->input("password") ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, "password_repeat")->input("password") ?>
    </div>

<?= Html::submitButton("Cadastrar", ["class" => "btn btn-primary"]) ?>

<?php $form->end() ?>