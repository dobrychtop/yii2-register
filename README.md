Erik Ieger Dobrychtop - Teste Executiva

Coloquei no bitbucket caso algum problema.
https://bitbucket.org/dobrychtop/yii2-register

Vou por no repositório pessoal caso desejam testar.
http://erikieger.com.br/testeExecutiva/yii2-register/web/index.php

Dados do banco de dados (pode ser encontrado no /config/db.php)
    'dsn' => 'pgsql:host=localhost;dbname=executiva',
    'username' => 'postgres',
    'password' => '',
    'charset' => 'utf8',

Para conseguir realizar o cadastro é necessário ter o banco de dados Ok, por isto coloquei um arquivo de create tables e insert.

Para isto é preciso criar a tabela paises e usuário e popular a tabela paises.
Segue os Script.

1º Crie a tabela Paises

-- Table: paises

-- DROP TABLE paises;

CREATE TABLE paises
(
  iso character varying(2) NOT NULL,
  iso3 character varying(3) NOT NULL,
  numcode smallint,
  nome character varying(255) NOT NULL,
  fuso_horario character varying NOT NULL,
  CONSTRAINT paises_pkey PRIMARY KEY (iso)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE paises
  OWNER TO postgres;

2º Crie a tabela usuário
-- Table: usuario

-- DROP TABLE usuario;

CREATE TABLE usuario
(
  usu_id serial NOT NULL, -- ID do Usuário
  usu_login character varying(30) NOT NULL, -- Login do Usuário
  usu_email character varying(50) NOT NULL, -- Email do Usuário
  usu_senha character varying(100) NOT NULL, -- Senha do Usuário
  usu_fuso_horario character varying(50) NOT NULL, -- Fuso horário do Usuário
  usu_pais character varying(2) NOT NULL, -- País do Usuário
  CONSTRAINT pk_usuario PRIMARY KEY (usu_id),
  CONSTRAINT fk_pais FOREIGN KEY (usu_pais)
      REFERENCES paises (iso) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE usuario
  OWNER TO postgres;
COMMENT ON TABLE usuario
  IS 'Tabela Usuário';
COMMENT ON COLUMN usuario.usu_id IS 'ID do Usuário
';
COMMENT ON COLUMN usuario.usu_login IS 'Login do Usuário';
COMMENT ON COLUMN usuario.usu_email IS 'Email do Usuário';
COMMENT ON COLUMN usuario.usu_senha IS 'Senha do Usuário';
COMMENT ON COLUMN usuario.usu_fuso_horario IS 'Fuso horário do Usuário';
COMMENT ON COLUMN usuario.usu_pais IS 'País do Usuário';

3º Popule a tabela Paises, segue abaixo um script de insert.
INSERT INTO paises VALUES ('AW', 'ABW', '533', 'Aruba', '+1');
INSERT INTO paises VALUES ('AU', 'AUS', '036', 'Austrália', '+1');
INSERT INTO paises VALUES ('AT', 'AUT', '040', 'Áustria', '+1');
INSERT INTO paises VALUES ('AZ', 'AZE', '031', 'Azerbeijão', '+1');
INSERT INTO paises VALUES ('BS', 'BHS', '044', 'Bahamas', '+1');
INSERT INTO paises VALUES ('BH', 'BHR', '048', 'Bahrain', '+1');
INSERT INTO paises VALUES ('BD', 'BGD', '050', 'Bangladesh', '+1');
INSERT INTO paises VALUES ('BB', 'BRB', '052', 'Barbados', '+1');
INSERT INTO paises VALUES ('BE', 'BEL', '056', 'Bélgica', '+1');
INSERT INTO paises VALUES ('BZ', 'BLZ', '084', 'Belize', '+1');
INSERT INTO paises VALUES ('BJ', 'BEN', '204', 'Benin', '+1');
INSERT INTO paises VALUES ('BM', 'BMU', '060', 'Bermuda', '+1');
INSERT INTO paises VALUES ('BY', 'BLR', '112', 'Bielo-Rússia', '+1');
INSERT INTO paises VALUES ('BO', 'BOL', '068', 'Bolívia', '+1');
INSERT INTO paises VALUES ('BA', 'BIH', '070', 'Bósnia-Herzegovina', '+1');
INSERT INTO paises VALUES ('BW', 'BWA', '072', 'Botswana', '+1');
INSERT INTO paises VALUES ('BV', 'BVT', '074', 'Bouvet, Ilha', '+1');
INSERT INTO paises VALUES ('BR', 'BRA', '076', 'Brasil', '+1');
INSERT INTO paises VALUES ('BN', 'BRN', '096', 'Brunei', '+1');
INSERT INTO paises VALUES ('BG', 'BGR', '100', 'Bulgária', '+1');
INSERT INTO paises VALUES ('BF', 'BFA', '854', 'Burkina Faso', '+1');


Os tres scripts vao estar em arquivos separados na pasta do projeto também caso não consiga copiar por aqui.




Abaixo segue o readme do Yii2 template Básico.


Yii 2 Basic Project Template
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.
