<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\FormCadastro;
use app\models\LoginForm;
use app\models\Pais;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCadastro()
    {
        if(!Yii::$app->user->isGuest){
            return $this->render('index');
        }


        //Instanciando o model
        $model = new FormCadastro();

        //Mensagem que vai mostrar de acordo com o resultado do cadastro
        $msg = null;

        //Validação do Ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()))
        {
            if($model->validate())
            {

                $table = new Users;
                $table->usu_login = $model->usu_login;
                $table->usu_email = $model->usu_email;
                $table->usu_pais = $model->usu_pais;
                //Add manualmente
//                $table->usu_senha = crypt($model->usu_senha, Yii::$app->params["salt"]);
                $table->usu_senha = $model->usu_senha;

                $query = Pais::find()
                    ->where(['=', 'iso', $model->usu_pais])
                    ->one();

                $table->usu_fuso_horario = $query->fuso_horario;

                if ($table->insert())
                {
                    $model->usu_login = null;
                    $model->usu_email = null;
                    $model->usu_senha = null;
                    $model->password_repeat = null;
                    $model->usu_fuso_horario = null;

                    $msg = "Parabéns, agora você pode entrar no sistema.";
                }
                else
                {
                    $msg = "Ocorreu um erro no cadastro";
                }

            }
            else
            {
                $model->getErrors();
            }
        }
        return $this->render("cadastro", ["model" => $model, "msg" => $msg]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
